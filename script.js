// AJAX это Асинхронный JavaScript и XML. 
// Это набор методов веб-разработки, 
// которые позволяют веб-приложениям работать асинхронно — 
// обрабатывать любые запросы к серверу в фоновом режиме.
// И JavaScript, и XML работают асинхронно в AJAX. 
// В результате любое веб-приложение, использующее AJAX, 
// может отправлять и извлекать данные с сервера без необходимости 
// перезагрузки всей страницы.



const postEl = document.createElement('div')
document.body.append(postEl)

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => {
    return response.json()
  })
  .then(data => {
    const list = data.map(({ characters, episodeId, name, openingCrawl }) => {
      const promises = characters.map(link => fetch(link).then(response => response.json()))
      Promise.all(promises).then(charactersObject => {
        const actorsName = charactersObject.map(({ name }) => {
          const span = document.createElement('span')
          span.innerText = `${name}, `;
          return span
        })
        div.append(...actorsName)
      })
      const div = document.createElement('div')
      const film = document.createElement('h2')
      const episode = document.createElement('h5')
      const filmDescription = document.createElement('p')
      const actorsList = document.createElement('h5')
      actorsList.innerText = "Актёры:";
      film.innerText = name;
      episode.innerText = `Эпизод №: ${episodeId}.`;
      filmDescription.innerText = openingCrawl;
      div.append(film, episode, filmDescription, actorsList)
      return div
    })
    postEl.prepend(...list)
  })